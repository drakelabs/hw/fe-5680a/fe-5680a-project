EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "FE-5680 Power Supply"
Date "2020-01-28"
Rev "1.9"
Comp "Drakelabs"
Comment1 "Dual-Secondary DC power for heater and logic"
Comment2 "Secondary 1 for logic via RS-232"
Comment3 "Secondary 2 for 3A heater"
Comment4 ""
$EndDescr
Text Notes 2450 850  0    108  ~ 0
Power Supply
$Comp
L Device:D_Bridge_-A+A D?
U 1 1 5DFB82D2
P 7110 1910
AR Path="/5DFB82D2" Ref="D?"  Part="1" 
AR Path="/5DFB25B0/5DFB82D2" Ref="D1"  Part="1" 
F 0 "D1" H 7070 1910 50  0000 L CNN
F 1 "D_Bridge_-A+A" H 7190 2110 50  0000 L CNN
F 2 "" H 7110 1910 50  0001 C CNN
F 3 "~" H 7110 1910 50  0001 C CNN
	1    7110 1910
	1    0    0    -1  
$EndComp
Wire Wire Line
	6660 2010 6660 1610
Wire Wire Line
	6660 1610 7110 1610
Wire Wire Line
	6810 1910 6810 2310
Wire Wire Line
	2780 5190 2380 5190
Wire Wire Line
	2080 5190 1780 5190
$Comp
L Diode:1N4002 D?
U 1 1 5DFB82E3
P 2230 5190
AR Path="/5DFB82E3" Ref="D?"  Part="1" 
AR Path="/5DFB25B0/5DFB82E3" Ref="D3"  Part="1" 
F 0 "D3" H 2230 5406 50  0000 C CNN
F 1 "1N4002" H 2230 5315 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2230 5015 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2230 5190 50  0001 C CNN
	1    2230 5190
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM317_TO3 U?
U 1 1 5DFB82E9
P 2230 5640
AR Path="/5DFB82E9" Ref="U?"  Part="1" 
AR Path="/5DFB25B0/5DFB82E9" Ref="U2"  Part="1" 
F 0 "U2" H 2230 5882 50  0000 C CNN
F 1 "LM317_TO3" H 2230 5791 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-3" H 2230 5840 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm317.pdf" H 2230 5640 50  0001 C CNN
	1    2230 5640
	1    0    0    -1  
$EndComp
Wire Wire Line
	1780 5640 1930 5640
Wire Wire Line
	1780 5190 1780 5640
Wire Wire Line
	2530 5640 2630 5640
Wire Wire Line
	2780 5640 2780 5190
Connection ~ 2780 5640
Wire Wire Line
	2780 5640 3080 5640
$Comp
L Diode:1N4002 D?
U 1 1 5DFB82F5
P 2630 5990
AR Path="/5DFB82F5" Ref="D?"  Part="1" 
AR Path="/5DFB25B0/5DFB82F5" Ref="D4"  Part="1" 
F 0 "D4" V 2584 6069 50  0000 L CNN
F 1 "1N4002" V 2675 6069 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2630 5815 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2630 5990 50  0001 C CNN
	1    2630 5990
	0    1    1    0   
$EndComp
Wire Wire Line
	2630 5840 2630 5640
Connection ~ 2630 5640
Wire Wire Line
	2630 5640 2780 5640
Wire Wire Line
	2230 5940 2230 6290
Wire Wire Line
	2630 6140 2630 6290
Wire Wire Line
	2630 6290 2230 6290
Wire Wire Line
	2230 6290 2230 6390
Connection ~ 2230 6290
$Comp
L Device:CP1_Small C?
U 1 1 5DFB8303
P 2230 6490
AR Path="/5DFB8303" Ref="C?"  Part="1" 
AR Path="/5DFB25B0/5DFB8303" Ref="C4"  Part="1" 
F 0 "C4" H 2321 6536 50  0000 L CNN
F 1 "CP1_Small" H 2321 6445 50  0000 L CNN
F 2 "" H 2230 6490 50  0001 C CNN
F 3 "~" H 2230 6490 50  0001 C CNN
	1    2230 6490
	1    0    0    -1  
$EndComp
Wire Wire Line
	3080 5640 3080 5840
$Comp
L Device:R_Small_US R?
U 1 1 5DFB830B
P 3080 5940
AR Path="/5DFB830B" Ref="R?"  Part="1" 
AR Path="/5DFB25B0/5DFB830B" Ref="R1"  Part="1" 
F 0 "R1" H 3148 5986 50  0000 L CNN
F 1 "R_Small_US" H 3148 5895 50  0000 L CNN
F 2 "" H 3080 5940 50  0001 C CNN
F 3 "~" H 3080 5940 50  0001 C CNN
	1    3080 5940
	1    0    0    -1  
$EndComp
Wire Wire Line
	3080 6040 3080 6290
Wire Wire Line
	3080 6290 2630 6290
Connection ~ 2630 6290
Wire Wire Line
	3080 6290 3080 6390
Connection ~ 3080 6290
$Comp
L Device:R_Small_US R?
U 1 1 5DFB8316
P 3080 6490
AR Path="/5DFB8316" Ref="R?"  Part="1" 
AR Path="/5DFB25B0/5DFB8316" Ref="R2"  Part="1" 
F 0 "R2" H 3148 6536 50  0000 L CNN
F 1 "R_Small_US" H 3148 6445 50  0000 L CNN
F 2 "" H 3080 6490 50  0001 C CNN
F 3 "~" H 3080 6490 50  0001 C CNN
	1    3080 6490
	1    0    0    -1  
$EndComp
Wire Wire Line
	2230 6590 2230 6690
Wire Wire Line
	2230 6690 3080 6690
Wire Wire Line
	3080 6590 3080 6690
Text HLabel 3580 5640 2    50   Output ~ 0
Logic_5VDC
NoConn ~ 4070 2410
$Comp
L Device:CP1_Small C2
U 1 1 5DFC890B
P 8370 2120
F 0 "C2" H 8410 2040 50  0000 L CNN
F 1 "CP" H 8400 2200 50  0000 L CNN
F 2 "" H 8370 2120 50  0001 C CNN
F 3 "~" H 8370 2120 50  0001 C CNN
	1    8370 2120
	1    0    0    -1  
$EndComp
Text HLabel 3580 6690 2    50   Output ~ 0
Logic_GND
Wire Wire Line
	4070 2910 6280 2910
Wire Wire Line
	3080 5640 3580 5640
Connection ~ 3080 5640
Wire Wire Line
	3580 6690 3080 6690
Connection ~ 3080 6690
Wire Wire Line
	4070 2210 7110 2210
Wire Wire Line
	1070 2310 1070 2110
Wire Wire Line
	1070 2610 1070 2810
Wire Wire Line
	1070 2110 1220 2110
Wire Wire Line
	1070 2810 1220 2810
Wire Wire Line
	1070 2110 770  2110
Connection ~ 1070 2110
Wire Wire Line
	1070 2810 770  2810
Connection ~ 1070 2810
Wire Wire Line
	4070 2010 6660 2010
Text Label 4070 2010 0    50   ~ 0
+16VAC
Text Label 4070 2210 0    50   ~ 0
+0VAC
Text Label 4070 2510 0    50   ~ 0
+15.5VAC
Text Label 4070 2910 0    50   ~ 0
-15.5VAC
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E2866F6
P 1200 1150
F 0 "#FLG0101" H 1200 1225 50  0001 C CNN
F 1 "PWR_FLAG" H 1200 1323 50  0000 C CNN
F 2 "" H 1200 1150 50  0001 C CNN
F 3 "~" H 1200 1150 50  0001 C CNN
	1    1200 1150
	1    0    0    -1  
$EndComp
$Comp
L power:NEUT #PWR0101
U 1 1 5E286D1A
P 1200 1350
F 0 "#PWR0101" H 1200 1200 50  0001 C CNN
F 1 "NEUT" H 1215 1523 50  0000 C CNN
F 2 "" H 1200 1350 50  0001 C CNN
F 3 "" H 1200 1350 50  0001 C CNN
	1    1200 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 1150 1200 1350
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E288910
P 750 1150
F 0 "#FLG0102" H 750 1225 50  0001 C CNN
F 1 "PWR_FLAG" H 750 1323 50  0000 C CNN
F 2 "" H 750 1150 50  0001 C CNN
F 3 "~" H 750 1150 50  0001 C CNN
	1    750  1150
	1    0    0    -1  
$EndComp
$Comp
L power:LINE #PWR0102
U 1 1 5E28B8B2
P 750 1350
F 0 "#PWR0102" H 750 1200 50  0001 C CNN
F 1 "LINE" H 765 1523 50  0000 C CNN
F 2 "" H 750 1350 50  0001 C CNN
F 3 "" H 750 1350 50  0001 C CNN
	1    750  1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	750  1150 750  1350
Wire Wire Line
	770  2010 770  2110
Wire Wire Line
	770  2710 770  2810
$Comp
L Device:C C1
U 1 1 5E2202C5
P 1070 2460
F 0 "C1" H 1185 2506 50  0000 L CNN
F 1 "C" H 1185 2415 50  0000 L CNN
F 2 "" H 1108 2310 50  0001 C CNN
F 3 "~" H 1070 2460 50  0001 C CNN
	1    1070 2460
	1    0    0    -1  
$EndComp
$Comp
L power:LINE #PWR0104
U 1 1 5E291DD9
P 770 2710
F 0 "#PWR0104" H 770 2560 50  0001 C CNN
F 1 "LINE" H 785 2883 50  0000 C CNN
F 2 "" H 770 2710 50  0001 C CNN
F 3 "" H 770 2710 50  0001 C CNN
	1    770  2710
	1    0    0    -1  
$EndComp
$Comp
L power:NEUT #PWR0103
U 1 1 5E29020B
P 770 2010
F 0 "#PWR0103" H 770 1860 50  0001 C CNN
F 1 "NEUT" H 785 2183 50  0000 C CNN
F 2 "" H 770 2010 50  0001 C CNN
F 3 "" H 770 2010 50  0001 C CNN
	1    770  2010
	1    0    0    -1  
$EndComp
$Comp
L DrakeLabs:Triad_F198U T1
U 1 1 5DFF1B3A
P 2620 2460
F 0 "T1" H 2693 3207 50  0000 C CNN
F 1 "Triad_F198U" H 2693 3116 50  0000 C CNN
F 2 "" H 2630 2900 50  0001 C CNN
F 3 "https://catalog.triadmagnetics.com/Asset/F-198U.pdf" H 2630 2900 50  0001 C CNN
F 4 "115V 50/60 Hz" H 2070 2410 50  0000 C CNN "Primary"
F 5 "32.0VCT 1A" H 3270 2660 50  0000 C CNN "Secondary 1"
F 6 "15.5VCT 6A" H 3270 2260 50  0000 C CNN "Secondary 2"
F 7 "5% Typ @ full load to no load" H 2670 960 39  0001 C CNN "V Reg"
F 8 "35C Typ (45C MAX allowed)" H 2620 1160 39  0001 C CNN "Temp RIse"
F 9 "UL Class B (130C)" H 2620 1460 39  0001 C CNN "Certification"
F 10 "1500V hipot tested between pri/sec, pri/sec and core" H 2620 1060 39  0001 C CNN "Insulation"
F 11 "Compliant as of 2005-02 to 2011/65/EU" H 2620 1360 39  0001 C CNN "RoHS"
	1    2620 2460
	1    0    0    -1  
$EndComp
NoConn ~ 4070 2710
$Comp
L power:GND #PWR01
U 1 1 5E29CB74
P 9740 2410
F 0 "#PWR01" H 9740 2160 50  0001 C CNN
F 1 "GND" H 9745 2237 50  0000 C CNN
F 2 "" H 9740 2410 50  0001 C CNN
F 3 "" H 9740 2410 50  0001 C CNN
	1    9740 2410
	1    0    0    -1  
$EndComp
Wire Wire Line
	9740 2410 9740 2310
Wire Wire Line
	1780 5640 1580 5640
Connection ~ 1780 5640
Wire Wire Line
	2230 6690 2230 6790
Connection ~ 2230 6690
$Comp
L power:GND #PWR03
U 1 1 5E2B89AB
P 2230 6790
F 0 "#PWR03" H 2230 6540 50  0001 C CNN
F 1 "GND" H 2235 6617 50  0000 C CNN
F 2 "" H 2230 6790 50  0001 C CNN
F 3 "" H 2230 6790 50  0001 C CNN
	1    2230 6790
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5E2B9136
P 1580 5640
F 0 "#FLG01" H 1580 5715 50  0001 C CNN
F 1 "PWR_FLAG" H 1580 5813 50  0000 C CNN
F 2 "" H 1580 5640 50  0001 C CNN
F 3 "~" H 1580 5640 50  0001 C CNN
	1    1580 5640
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR02
U 1 1 5E2B9999
P 1580 5640
F 0 "#PWR02" H 1580 5490 50  0001 C CNN
F 1 "VCC" H 1595 5813 50  0000 C CNN
F 2 "" H 1580 5640 50  0001 C CNN
F 3 "" H 1580 5640 50  0001 C CNN
	1    1580 5640
	1    0    0    -1  
$EndComp
Connection ~ 1580 5640
Wire Wire Line
	9740 1910 9740 1810
$Comp
L power:VCC #PWR04
U 1 1 5E2BBABB
P 9740 1810
F 0 "#PWR04" H 9740 1660 50  0001 C CNN
F 1 "VCC" H 9755 1983 50  0000 C CNN
F 2 "" H 9740 1810 50  0001 C CNN
F 3 "" H 9740 1810 50  0001 C CNN
	1    9740 1810
	1    0    0    -1  
$EndComp
Text Notes 2130 4790 0    108  ~ 0
+5VDC LM317
Text Notes 7180 4030 0    50   ~ 0
AC+
Wire Wire Line
	7830 4030 7830 4180
Connection ~ 7830 4180
Wire Wire Line
	7830 4180 7830 4380
Text Notes 7180 4380 0    50   ~ 0
AC-
Text Notes 7860 4180 0    50   ~ 0
DC+
Text Notes 7850 4830 0    50   ~ 0
DC-
Wire Wire Line
	7830 4680 7830 4830
Connection ~ 7830 4830
Wire Wire Line
	7830 4830 7830 4980
Wire Wire Line
	7530 4980 7380 4980
Wire Wire Line
	7380 4980 7380 4380
Wire Wire Line
	7530 4380 7380 4380
Connection ~ 7380 4380
Text Notes 6690 3800 0    50   ~ 0
Discrete power diode bridge\n
Wire Notes Line
	1180 4490 4130 4490
Wire Notes Line
	4130 4490 4130 7140
Wire Notes Line
	4130 7140 1180 7140
Wire Notes Line
	1180 7140 1180 4490
Text Notes 6390 3680 0    35   Italic 0
App Note: Discrete power diodes may be a superior choice,\nparticularly for the > 3A heater circuit, where more passive\ncooling may be possible.
$Comp
L Device:C C5
U 1 1 5E324E19
P 8690 2110
F 0 "C5" H 8805 2156 50  0000 L CNN
F 1 "C" H 8805 2065 50  0000 L CNN
F 2 "" H 8728 1960 50  0001 C CNN
F 3 "~" H 8690 2110 50  0001 C CNN
	1    8690 2110
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5E32550C
P 8940 1910
F 0 "L1" V 9130 1910 50  0000 C CNN
F 1 "L" V 9039 1910 50  0000 C CNN
F 2 "" H 8940 1910 50  0001 C CNN
F 3 "~" H 8940 1910 50  0001 C CNN
	1    8940 1910
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9090 1910 9190 1910
$Comp
L Device:C C6
U 1 1 5E326347
P 9190 2110
F 0 "C6" H 9305 2156 50  0000 L CNN
F 1 "C" H 9305 2065 50  0000 L CNN
F 2 "" H 9228 1960 50  0001 C CNN
F 3 "~" H 9190 2110 50  0001 C CNN
	1    9190 2110
	1    0    0    -1  
$EndComp
Wire Wire Line
	8690 1960 8690 1910
Connection ~ 8690 1910
Wire Wire Line
	8690 1910 8790 1910
Wire Wire Line
	8690 2260 8690 2310
Connection ~ 8690 2310
Wire Wire Line
	8690 2310 9190 2310
Wire Wire Line
	9190 1960 9190 1910
Connection ~ 9190 1910
Wire Wire Line
	9190 1910 9740 1910
Wire Wire Line
	9190 2260 9190 2310
Connection ~ 9190 2310
Wire Wire Line
	9190 2310 9740 2310
Wire Notes Line
	9390 2360 8590 2360
Text Notes 8660 1660 0    35   Italic 0
Π LP\n(subject to change)
Wire Notes Line
	8590 2360 8590 1510
Wire Notes Line
	8590 1510 9390 1510
Wire Notes Line
	9390 1510 9390 2360
$Comp
L Connector:TestPoint_2Pole TP1
U 1 1 5E34C90D
P 7900 2110
F 0 "TP1" V 7810 1930 50  0000 L CNN
F 1 "TestPoint" V 8150 1940 50  0000 L CNN
F 2 "" H 7900 2110 50  0001 C CNN
F 3 "~" H 7900 2110 50  0001 C CNN
	1    7900 2110
	0    1    1    0   
$EndComp
Wire Wire Line
	8370 1910 8370 2020
Connection ~ 8370 2020
Wire Wire Line
	8370 2020 8370 2030
Connection ~ 8370 1910
Wire Wire Line
	8370 1910 8690 1910
Wire Wire Line
	8370 2220 8370 2310
Connection ~ 8370 2310
Wire Wire Line
	8370 2310 8690 2310
Text Notes 9180 5520 0    35   ~ 0
Test Points\n(A) Initial output of diode bridge, pre-filter. Dependent on\n    load. Without load, should reflect initial DC values for rail 1 (TTL).
Wire Notes Line
	11200 5320 9100 5320
Wire Notes Line
	9100 5320 9100 6360
Wire Notes Line
	6380 3480 8050 3480
Wire Notes Line
	11210 5320 11210 6360
Wire Notes Line
	11210 6360 9100 6360
Wire Wire Line
	7470 4680 7530 4680
Wire Wire Line
	7470 4680 7470 4030
Connection ~ 7470 4030
Wire Wire Line
	7470 4030 7530 4030
Wire Wire Line
	6330 2510 4070 2510
Wire Wire Line
	6330 2510 6330 4030
Wire Wire Line
	6280 2910 6280 4380
Wire Wire Line
	6280 4380 7380 4380
Wire Wire Line
	7830 4180 8590 4180
Wire Wire Line
	7830 4830 8590 4830
$Comp
L Device:CP1 C7
U 1 1 5E3AF9C9
P 8590 4510
F 0 "C7" H 8705 4556 50  0000 L CNN
F 1 "CP1" H 8705 4465 50  0000 L CNN
F 2 "" H 8590 4510 50  0001 C CNN
F 3 "~" H 8590 4510 50  0001 C CNN
	1    8590 4510
	1    0    0    -1  
$EndComp
Wire Wire Line
	8590 4360 8590 4180
Wire Wire Line
	8590 4660 8590 4830
Text HLabel 8800 4180 2    50   Output ~ 0
Heater_15.5VDC
Text HLabel 8800 4830 2    50   Output ~ 0
Heater_GND
Wire Wire Line
	8800 4830 8590 4830
Connection ~ 8590 4830
Wire Wire Line
	8590 4180 8800 4180
Connection ~ 8590 4180
Connection ~ 7900 1910
Wire Wire Line
	7900 1910 8370 1910
Connection ~ 7900 2310
Wire Wire Line
	7900 2310 8370 2310
Wire Wire Line
	7410 1910 7900 1910
Wire Wire Line
	6810 2310 7900 2310
Text HLabel 2510 2340 2    50   Output ~ 0
Logic_5VDC
Wire Wire Line
	6330 4030 7470 4030
Text Notes 2490 1130 0    78   ~ 0
Dual Secondary\nBoth Secondaries Center-Tapped
$Comp
L DrakeLabs:6A10-T D?
U 1 1 5E453C22
P 7830 4030
F 0 "D?" V 7708 3880 35  0000 C CNN
F 1 "6A10-T" H 7830 3980 35  0000 C CNN
F 2 "" H 7830 4030 35  0001 C CNN
F 3 "https://www.mouser.com/datasheet/2/115/ds28009-70454.pdf" H 7830 4030 35  0001 C CNN
	1    7830 4030
	0    1    1    0   
$EndComp
$Comp
L DrakeLabs:6A10-T D?
U 1 1 5E456D6F
P 7830 4380
F 0 "D?" V 7708 4230 35  0000 C CNN
F 1 "6A10-T" H 7830 4330 35  0000 C CNN
F 2 "" H 7830 4380 35  0001 C CNN
F 3 "https://www.mouser.com/datasheet/2/115/ds28009-70454.pdf" H 7830 4380 35  0001 C CNN
	1    7830 4380
	0    1    1    0   
$EndComp
$Comp
L DrakeLabs:6A10-T D?
U 1 1 5E459361
P 7530 4680
F 0 "D?" V 7642 4530 35  0000 C CNN
F 1 "6A10-T" H 7530 4630 35  0000 C CNN
F 2 "" H 7530 4680 35  0001 C CNN
F 3 "https://www.mouser.com/datasheet/2/115/ds28009-70454.pdf" H 7530 4680 35  0001 C CNN
	1    7530 4680
	0    -1   -1   0   
$EndComp
$Comp
L DrakeLabs:6A10-T D?
U 1 1 5E45BC11
P 7530 4980
F 0 "D?" V 7642 4830 35  0000 C CNN
F 1 "6A10-T" H 7530 4930 35  0000 C CNN
F 2 "" H 7530 4980 35  0001 C CNN
F 3 "https://www.mouser.com/datasheet/2/115/ds28009-70454.pdf" H 7530 4980 35  0001 C CNN
	1    7530 4980
	0    -1   -1   0   
$EndComp
Wire Notes Line
	6380 3480 6380 3830
Wire Notes Line
	6380 3830 8060 3830
Wire Notes Line
	8060 3830 8060 3480
$EndSCHEMATC
