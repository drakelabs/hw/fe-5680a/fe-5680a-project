EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "FE-5680A PHY"
Date "2020-01-28"
Rev "1.9"
Comp "Drakelabs"
Comment1 "Contains trim, physics box, and DB9 I/0."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1600 2100 0    50   Input ~ 0
Heater_15.5VDC
Wire Wire Line
	1600 2100 1900 2100
Text HLabel 1600 2850 0    50   Input ~ 0
Logic_5VDC
Wire Wire Line
	1600 2850 1900 2850
$Comp
L DrakeLabs:FE-5680A-Rb-Osc G1
U 1 1 5DFF431D
P 2650 2450
F 0 "G1" H 2650 3465 50  0000 C CNN
F 1 "FE-5680A-Rb-Osc" H 2650 3374 50  0000 C CNN
F 2 "" H 2650 2450 50  0001 C CNN
F 3 "" H 2650 2450 50  0001 C CNN
	1    2650 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2750 3600 2750
Wire Wire Line
	3600 2750 3600 2400
Wire Wire Line
	3400 3000 3750 3000
Wire Wire Line
	3750 3000 3750 2600
Text Label 4050 2400 0    50   ~ 0
RS232-RX
Text Label 4050 2600 0    50   ~ 0
RS232-TX
Wire Wire Line
	3400 2000 3600 2000
Wire Wire Line
	3600 2000 3600 2200
Text Label 4000 2200 0    50   ~ 0
RS232-GND
Wire Notes Line
	800  1300 4500 1300
Wire Notes Line
	4500 1300 4500 3550
Wire Notes Line
	4500 3550 800  3550
Wire Notes Line
	800  3550 800  1300
Text Notes 900  1450 0    69   ~ 0
FE-5680 Body
Text Label 5950 2900 0    50   ~ 0
RS232-RX
Text Label 5950 3000 0    50   ~ 0
RS232-TX
Text Label 5950 3100 0    50   ~ 0
RS232-GND
Wire Wire Line
	3400 2500 3500 2500
Wire Wire Line
	3500 2500 3500 3800
Text Notes 3400 4050 0    35   ~ 0
To distribution amplifier\nBNC and SMA
$Sheet
S 5500 4900 1800 1200
U 5E0BFA9A
F0 "Teensy Microcontroller" 50
F1 "Teensy_Microcontroller.sch" 50
F2 "RS232-GND" I R 7300 5000 50 
F3 "RS232-TX" I R 7300 5100 50 
F4 "RS232-RX" I R 7300 5200 50 
$EndSheet
$Sheet
S 2650 4850 1800 1300
U 5E0C40A9
F0 "10MHz and 1PPS Distribution" 50
F1 "DistroAmp.sch" 50
F2 "1pps" I R 4450 4900 50 
F3 "RF_OUT_10MHz" I R 4450 5100 50 
$EndSheet
Wire Wire Line
	3500 3800 4800 3800
Wire Wire Line
	4800 3800 4800 5100
Wire Wire Line
	4800 5100 4450 5100
Wire Wire Line
	3400 3900 4700 3900
Wire Wire Line
	4700 3900 4700 4900
Wire Wire Line
	4700 4900 4450 4900
Wire Wire Line
	3400 2250 3400 3900
Wire Notes Line
	3350 3750 4100 3750
Wire Notes Line
	4100 3750 4100 4100
Wire Notes Line
	4100 4100 3350 4100
Wire Notes Line
	3350 4100 3350 3750
Wire Wire Line
	1450 2350 1900 2350
$Comp
L pspice:0 #GND03
U 1 1 5E23EF47
P 1450 2400
F 0 "#GND03" H 1450 2300 50  0001 C CNN
F 1 "0" H 1450 2489 50  0000 C CNN
F 2 "" H 1450 2400 50  0001 C CNN
F 3 "~" H 1450 2400 50  0001 C CNN
	1    1450 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2400 1450 2350
NoConn ~ 1900 2600
Wire Wire Line
	4900 2200 4900 3100
Wire Wire Line
	3600 2200 4900 2200
Wire Wire Line
	4800 2600 4800 3000
Wire Wire Line
	3750 2600 4800 2600
Wire Wire Line
	5000 2400 5000 2900
Wire Wire Line
	3600 2400 5000 2400
Wire Wire Line
	4900 3100 7700 3100
Wire Wire Line
	7700 3100 7700 5000
Wire Wire Line
	7800 3000 7800 5100
Wire Wire Line
	4800 3000 7800 3000
Wire Wire Line
	7900 2900 7900 5200
Wire Wire Line
	5000 2900 7900 2900
Wire Wire Line
	7300 5000 7700 5000
Wire Wire Line
	7300 5100 7800 5100
Wire Wire Line
	7300 5200 7900 5200
$EndSCHEMATC
