EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Teensy Microcontroller IO"
Date "2020-01-28"
Rev "1.9"
Comp "Drakelabs"
Comment1 "Teensy 4.0 Microcontroller with code logging, commands via RS232, etc. from PJRC"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3180 2990 2    50   ~ 0
RS232-TX
Text Label 3180 3140 2    50   ~ 0
RS232-RX
Text Label 3230 3290 2    50   ~ 0
RS232-GND
Wire Wire Line
	2680 3140 3420 3140
NoConn ~ 6700 2650
NoConn ~ 6700 2750
NoConn ~ 6700 2850
NoConn ~ 6700 3050
NoConn ~ 6700 3250
NoConn ~ 6700 3350
NoConn ~ 6700 3450
NoConn ~ 6700 3650
NoConn ~ 6700 3750
NoConn ~ 6700 3850
NoConn ~ 6700 3950
NoConn ~ 6700 4050
NoConn ~ 6700 4150
NoConn ~ 6700 4250
NoConn ~ 6700 4350
NoConn ~ 6700 4650
NoConn ~ 6700 5050
NoConn ~ 6700 5150
NoConn ~ 4500 2550
NoConn ~ 4500 2650
NoConn ~ 4500 2750
NoConn ~ 4500 2850
NoConn ~ 4500 2950
NoConn ~ 4500 3050
NoConn ~ 4500 3150
NoConn ~ 4500 3250
NoConn ~ 4500 3350
NoConn ~ 4500 3450
NoConn ~ 4500 3550
NoConn ~ 4500 3650
NoConn ~ 4500 3750
NoConn ~ 4500 4150
NoConn ~ 4500 4350
NoConn ~ 4500 4450
NoConn ~ 4500 4550
NoConn ~ 4500 4650
NoConn ~ 4500 4750
NoConn ~ 4500 4850
NoConn ~ 4500 4950
NoConn ~ 4500 5050
NoConn ~ 4500 5150
NoConn ~ 4500 2450
NoConn ~ 4500 3850
NoConn ~ 4500 3950
NoConn ~ 4500 4050
NoConn ~ 4500 4250
NoConn ~ 6700 2450
NoConn ~ 6700 2550
NoConn ~ 6700 2950
NoConn ~ 6700 3150
NoConn ~ 6700 3550
NoConn ~ 6700 4950
$Comp
L teensy:Teensy4.0 U1
U 1 1 5E0C2335
P 5600 3800
F 0 "U1" H 5600 5415 50  0000 C CNN
F 1 "Teensy4.0" H 5600 5324 50  0000 C CNN
F 2 "" H 5200 4000 50  0001 C CNN
F 3 "" H 5200 4000 50  0001 C CNN
	1    5600 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3420 3290 2680 3290
Wire Wire Line
	2680 2990 3430 2990
Wire Wire Line
	2000 1990 7000 1990
Wire Wire Line
	7000 4850 7000 6000
Wire Wire Line
	7000 6000 8000 6000
Wire Wire Line
	6700 4850 7000 4850
Wire Wire Line
	6700 4750 7000 4750
Wire Wire Line
	7000 1990 7000 4750
Connection ~ 7000 1990
Wire Wire Line
	7000 1990 8020 1990
Connection ~ 7000 6000
Wire Wire Line
	2000 6000 7000 6000
Text GLabel 2000 6000 0    35   Input ~ 0
GND
Text GLabel 2000 1990 0    35   Input ~ 0
5VDC
$EndSCHEMATC
