EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "FE-5680 Support"
Date "2020-01-28"
Rev "1.9"
Comp "Drakelabs"
Comment1 "Top level components."
Comment2 "The physics package, a microcontroller, power supply, and distribution amplifier."
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 10600 7650 0    50   ~ 0
1
$Sheet
S 1000 3500 3000 2000
U 5DFD8BA8
F0 "FE-5680 Package" 50
F1 "5680-phy.sch" 50
F2 "Heater_15.5VDC" I R 4000 4100 50 
F3 "Logic_5VDC" I R 4000 3700 50 
$EndSheet
$Sheet
S 1000 1000 3000 2000
U 5DFB25B0
F0 "FE-5680 Power supply" 50
F1 "power-supply.sch" 50
F2 "Logic_5VDC" O R 4000 1950 50 
F3 "Heater_15.5VDC" O R 4000 2350 50 
F4 "Logic_GND" O R 4000 2100 50 
F5 "Heater_GND" O R 4000 2500 50 
$EndSheet
Wire Wire Line
	4100 4100 4000 4100
Wire Wire Line
	4000 2350 4100 2350
Wire Wire Line
	4100 2350 4100 4100
Wire Wire Line
	4150 1950 4150 3700
Wire Wire Line
	4150 3700 4000 3700
Wire Wire Line
	4000 1950 4150 1950
Wire Wire Line
	4350 2100 4350 2200
Wire Wire Line
	4000 2100 4350 2100
Wire Wire Line
	4350 2500 4350 2600
Wire Wire Line
	4000 2500 4350 2500
$Comp
L pspice:0 #GND01
U 1 1 5E2402A3
P 4350 2200
F 0 "#GND01" H 4350 2100 50  0001 C CNN
F 1 "0" H 4350 2289 50  0000 C CNN
F 2 "" H 4350 2200 50  0001 C CNN
F 3 "~" H 4350 2200 50  0001 C CNN
	1    4350 2200
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND02
U 1 1 5E240931
P 4350 2600
F 0 "#GND02" H 4350 2500 50  0001 C CNN
F 1 "0" H 4350 2689 50  0000 C CNN
F 2 "" H 4350 2600 50  0001 C CNN
F 3 "~" H 4350 2600 50  0001 C CNN
	1    4350 2600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
