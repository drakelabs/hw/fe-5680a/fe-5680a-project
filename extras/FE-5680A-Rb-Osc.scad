// FE-5680A Rb Frequency Standard
// 3D Model and footprint for Kicad.
// Units: mm

package_X = 87.5;
package_Y = 124;
package_Z = 22.5;

foot_X = 101.5;
foot_Y = 137.5;
foot_Z = 1;

screw_width = 3;

foot_width = (foot_X - package_X) / 2;

module case() {
    // holds the physics package and supporting
    // TTL, etc.
    package_dims = [
        package_X,
        package_Y,
        package_Z
    ];
    cube (package_dims, center=true);
}

module foot() {
    // Flange for attaching to PCB
    foot_dims = [
        foot_X,
        foot_Y,
        foot_Z
    ];
    translate ([0, 0, (package_Z/-1 * 0.5)])
    cube (foot_dims, center=true);
    
}

module screwholes() {
    module screwhole () {
        $fn = 50;
        cylinder (
            (foot_Z*2)+2, screw_width/2, screw_width/2,
            center=true
        );
    }

    hole_offset_X = 24;
    hole_offset_Y = 26;
   
    difference() {
        foot();
        translate ([
            0,
            ((foot_Y * -1)/2) + (foot_width /2 ),
            (package_Z/-1 * 0.5)
        ]) screwhole ();
        echo (((foot_Y * -1)/2) + (foot_width /2 ));

        translate ([
            hole_offset_X * -1,
            ((foot_Y * -1)/2) + (foot_width /2 ),
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        // lower left corner
        translate ([
            hole_offset_X * -2,
            ((foot_Y * -1)/2) + (foot_width /2 ),
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        // lower right corner
        translate ([
            hole_offset_X * 2,
            ((foot_Y * -1)/2) + (foot_width /2 ),
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        // left side
        translate ([
            hole_offset_X * -2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X * -2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 2,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X * -2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 3,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X * -2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 4,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        // upper left corner
        translate ([
            hole_offset_X * -2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 5,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        // right side
        translate ([
            hole_offset_X * 2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X * 2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 2,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X * 2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 3,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X * 2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 4,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X * 2,
            ((
                (foot_Y * -1)/2) +
                (foot_width /2 )) + hole_offset_Y * 5,
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        // top row
        translate ([
            0,
            (foot_Y / 2) - (foot_width / 2),
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            hole_offset_X,
            (foot_Y / 2) - (foot_width / 2),
            (package_Z/-1 * 0.5)
        ]) screwhole ();

        translate ([
            0 - hole_offset_X,
            (foot_Y / 2) - (foot_width / 2),
            (package_Z/-1 * 0.5)
        ]) screwhole ();
    }
}

case();
//foot();

screwholes();


