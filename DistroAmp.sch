EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "RF Out 10Mhz and 1pps Distribution"
Date "2020-01-28"
Rev "1.9"
Comp "Drakelabs"
Comment1 "This module will divide the 10MHz and 1pps signal among several BNC and SMA connections."
Comment2 "Sets of outlets on the rear and front of chassis."
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2450 2700 1    50   Output ~ 0
RF_OUT_10MHz
Wire Wire Line
	2450 2700 2450 3000
Wire Wire Line
	2450 3000 5350 3000
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5E0DE065
P 5550 3000
F 0 "J1" H 5650 2975 50  0000 L CNN
F 1 "Conn_Coaxial" H 5650 2884 50  0000 L CNN
F 2 "" H 5550 3000 50  0001 C CNN
F 3 " ~" H 5550 3000 50  0001 C CNN
	1    5550 3000
	1    0    0    -1  
$EndComp
Text GLabel 5550 3200 2    50   Output ~ 0
GND
$EndSCHEMATC
